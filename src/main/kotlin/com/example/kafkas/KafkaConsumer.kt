package com.example.kafkas

import org.apache.commons.logging.LogFactory
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.core.log.LogAccessor
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component
import java.util.concurrent.CountDownLatch


@Component
class KafkaConsumer {

    var latch = CountDownLatch(1)
    var payload: String? = null

    private val logger = LogAccessor(LogFactory.getLog(this.javaClass))

    @KafkaListener(
        topics = ["\${spring.kafka.my-kafka-topic}"]
    )
    fun receive(consumerRecord: ConsumerRecord<*, *>) {
        logger.info("received payload=", consumerRecord);
        payload = consumerRecord.toString()
        latch.countDown()
    }

    fun resetLatch() {
        latch = CountDownLatch(1)
    }

    fun LogAccessor.info(
        message: String,
        payload: Any
    ) {
        info("$message payload: $payload")
    }
}

