package com.example.kafkas

import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

@Component
class KafkaListener {

    @KafkaListener(topics = ["myKafkaTopic"], groupId = "groupId")
    fun listener(data: Entity) {
        println("Listener received: $data")
    }
}