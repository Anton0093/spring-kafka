package com.example.kafkas

data class Entity(val name: String, val age: Int, val email: String)
