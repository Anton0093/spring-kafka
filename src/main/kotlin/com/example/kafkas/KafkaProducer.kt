package com.example.kafkas

import lombok.extern.slf4j.Slf4j
import org.apache.commons.logging.LogFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.log.LogAccessor
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component

@Slf4j
@Component
class KafkaProducer{

    @Autowired
    lateinit var kafkaTemplate: KafkaTemplate<String, String>

    private val logger = LogAccessor(LogFactory.getLog(this.javaClass))

    fun send(topic: String, payload: String) {
        logger.info("sending payload to topic= ", payload, topic)
        kafkaTemplate.send(topic, payload)
    }
}

fun LogAccessor.info(
    message: String,
    payload: String,
    topic: String
) {
    info("$message payload: $payload" + "topic $topic")
}