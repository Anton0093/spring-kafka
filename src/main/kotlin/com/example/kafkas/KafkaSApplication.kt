package com.example.kafkas

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KafkaSApplication

fun main(args: Array<String>) {
    runApplication<KafkaSApplication>(*args)
}
