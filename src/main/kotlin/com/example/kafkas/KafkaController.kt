package com.example.kafkas

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/kafka")
class KafkaController {

    @Autowired
    lateinit var kafkaTemplate: KafkaTemplate<String, Entity>

    @Value("\${spring.kafka.my-kafka-topic}")
    lateinit var topic: String

    @PostMapping
    fun post(@RequestBody entity: Entity): ResponseEntity<Entity> {
        val future = kafkaTemplate.send(topic, entity)

        println(future.get().producerRecord.value())
        println(future.get().producerRecord.topic())

        return ResponseEntity<Entity>(entity, HttpStatus.OK)
    }
}