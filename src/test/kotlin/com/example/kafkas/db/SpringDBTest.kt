package com.example.kafkas.db

import SpringBootTestWithDB
import com.example.kafkas.db.containers.DBContainerSetup
import com.example.kafkas.db.initializers.Initializer
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration

@SpringBootTest
@ExtendWith(DBContainerSetup::class)
@ContextConfiguration(initializers = [Initializer::class])
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class SpringDBTest {

    @Test
    fun contextLoads() {
        println("init")
    }
}
