package com.example.kafkas.db.containers

import org.apache.commons.logging.LogFactory
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.springframework.core.log.LogAccessor
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.utility.DockerImageName

class DBContainerSetup : BeforeAllCallback,
    AfterAllCallback {

    private val logger = LogAccessor(LogFactory.getLog(this.javaClass))


    override fun afterAll(context: ExtensionContext?) {
        postgreSQLContainer.stop()
    }

    override fun beforeAll(context: ExtensionContext?) {
        initPostgreSQLContainer()
    }


    companion object {
        lateinit var postgreSQLContainer: PostgreSQLContainer<Nothing>

        internal fun initPostgreSQLContainer() {
            val imageName = DockerImageName.parse("postgres:14.2-alpine")

            postgreSQLContainer = PostgreSQLContainer(imageName)
            postgreSQLContainer.apply {
                withDatabaseName("kb2test")
                withUsername("kb2")
                withPassword("kb2")
                withExposedPorts(5432)
            }
            postgreSQLContainer.start()
        }
    }
}

