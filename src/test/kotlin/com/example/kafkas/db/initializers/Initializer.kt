package com.example.kafkas.db.initializers

import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import com.example.kafkas.db.containers.DBContainerSetup

class Initializer : ApplicationContextInitializer<ConfigurableApplicationContext> {

    val postgres = DBContainerSetup.postgreSQLContainer

    override fun initialize(configurableApplicationContext: ConfigurableApplicationContext) {
        TestPropertyValues.of(
            "spring.datasource.url=" + postgres.jdbcUrl,
            "spring.datasource.username=" + postgres.username,
            "spring.datasource.password=" + postgres.password
        )
            .applyTo(configurableApplicationContext.environment)
    }
}