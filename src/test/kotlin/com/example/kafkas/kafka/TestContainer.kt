package com.example.kafkas.kafka

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import java.time.Duration

@SpringBootTest
@ExtendWith(ContainerSetup::class)
class KafkaProducerTest {

    @Test
    fun test() {
        val kafka = ContainerSetup.kafkaContainer

        val producer = KafkaProducer(
            mapOf(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG to kafka.bootstrapServers),
            StringSerializer(),
            StringSerializer()
        )

        val consumer = KafkaConsumer(
            mapOf(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG to kafka.bootstrapServers,
                ConsumerConfig.AUTO_OFFSET_RESET_CONFIG to "earliest",
                ConsumerConfig.GROUP_ID_CONFIG to "test-group-id"
            ),
            StringDeserializer(),
            StringDeserializer()
        ).apply { subscribe(listOf("myConsumerTopic")) }

        // Act
        producer.send(ProducerRecord("myConsumerTopic", "Hello there!"))
        producer.flush()

        // Assert
        assertEquals(consumer.poll(Duration.ofSeconds(3)).first().value(), "Hello there!")
    }
}