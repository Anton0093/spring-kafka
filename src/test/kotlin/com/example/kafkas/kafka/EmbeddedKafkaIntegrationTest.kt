//package com.example.kafkas.kafka
//
//import com.example.kafkas.KafkaConsumer
//import com.example.kafkas.KafkaProducer
//import org.assertj.core.api.Assertions.assertThat
//import org.hamcrest.CoreMatchers.containsString
//import org.junit.jupiter.api.Assertions.assertEquals
//import org.junit.jupiter.api.Assertions.assertTrue
//import org.junit.jupiter.api.Test
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.beans.factory.annotation.Value
//import org.springframework.boot.test.context.SpringBootTest
//import org.springframework.kafka.test.context.EmbeddedKafka
//import org.springframework.test.annotation.DirtiesContext
//import java.util.concurrent.TimeUnit
//
//
//@SpringBootTest
//@DirtiesContext
//@EmbeddedKafka(
//    partitions = 1,
//    brokerProperties = [
//        "listeners=PLAINTEXT://localhost:9092",
//        "port=9092"
//    ]
//)
//class EmbeddedKafkaIntegrationTest {
//
//    @Autowired
//    lateinit var consumer: KafkaConsumer
//
//    @Autowired
//    lateinit var producer: KafkaProducer
//
//    @Value("\${spring.kafka.my-kafka-topic}")
//    lateinit var topic: String
//
//    @Test
//    @Throws(Exception::class)
//    fun givenEmbeddedKafkaBroker_whenSendingWithSimpleProducer_thenMessageReceived() {
//        val data = "Sending with our own simple KafkaProducer"
//        producer.send(topic, data)
//
//        val messageConsumed: Boolean = consumer.latch.await(10, TimeUnit.SECONDS)
//
//        assertTrue(messageConsumed)
//    }
//}