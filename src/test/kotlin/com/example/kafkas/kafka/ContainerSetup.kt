package com.example.kafkas.kafka

import org.apache.commons.logging.LogFactory
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.springframework.core.log.LogAccessor
import org.testcontainers.containers.KafkaContainer
import org.testcontainers.utility.DockerImageName

class ContainerSetup: BeforeAllCallback,
    AfterAllCallback {

    private val logger = LogAccessor(LogFactory.getLog(this.javaClass))


    override fun afterAll(context: ExtensionContext?) {
        kafkaContainer.stop()
    }

    override fun beforeAll(context: ExtensionContext?) {
        initKafkaContainer()
    }


    companion object {
        lateinit var kafkaContainer: KafkaContainer

        internal fun initKafkaContainer() {
            val imageName = DockerImageName.parse("confluentinc/cp-kafka:7.0.0")

            kafkaContainer = KafkaContainer(imageName)
            kafkaContainer.start()
//            await until { kafkaContainer.isRunning }
        }
    }


}
